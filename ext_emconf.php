<?php

/**
 * Extension Manager/Repository config file for ext "toctoc_comments".
 *
 * Auto generated 11-03-2016 16:14
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 */

$EM_CONF[$_EXTKEY] = array(
	'title' => 'AJAX Social Network Components',
	'description' => 'AJAX-based commenting, review, rating and sharing system (jQuery) for TYPO3 4.3 to 7.x. 
		Comments or reviews can have webpage previews, pictures and pdfs attached. You can use BB-Codes, emojis and smilies. 
		Userpictures include gravatar-, Facebook- or fe_users-userpictures. 
		More plugin-modes for comments are available, a user center, a recent comments list, a report bad comment form and several charts. 
		Four looks for ratings by voting stars and Facebook-like iLikes, usage of scopes for categorized ratings. 
		For ratings there is a plugin-mode for top ratings. Many options for spam- and data-protection. AJAX runs with jQuery.
		ViewHelpers for extension news. 
		Comments, reviews or ratings can trigger records in other extensions DETAIL-views and LIST-views (LIST-view tt_news, news, modify all other pi1-extensions in PHP and implement a new marker or ViewHelper for comments and ratings in LIST-views).
		Includes full featured AJAX Login and Sign Up, including login with Facebook or Google+ account. 
		Backendmodule for comments, users and system control
		Works with TemplaVoila, tx_news, tt_news, tt_products, community, cwt_community and most of all other extensions.
		Designable from included LESS-model',
	'category' => 'plugin',
	'shy' => 1,
	'version' => '9.0.1',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => '',
	'createDirs' => 'uploads/tx_toctoccomments/temp, uploads/tx_toctoccomments/webpagepreview, uploads/tx_felogin',
	'modify_tables' => '',
	'clearcacheonload' => 1,
	'lockType' => '',
	'author' => 'Gisele Wendl',
	'author_email' => 'gisele.wendl@toctoc.ch',
	'author_company' => 'TocToc Internetmanagement',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
			'typo3' => '4.3.0-7.9.99',
			'php' => '5.3.7-7.9.99',
		),
	),
);

